# Harpia
UFRJ Nautilus autonomous drone.

# Table of Contents
1. [Installation](#installation)
2. [Usage](#usage)
3. [TroubleShoot](#troubleShoot)
4. [Source](#source)

# Installation

## Installing ArduPilot
First clone ardupilot

```
cd ~
git clone https://github.com/ArduPilot/ardupilot.git
cd ardupilot
git checkout Copter-3.6
git submodule update --init --recursive
```

Install the Dependencies
```
sudo apt install python-matplotlib python-serial python-wxgtk3.0 python-wxtools python-lxml python-scipy python-opencv ccache gawk python-pip python-pexpect
sudo pip install future pymavlink MAVProxy
```

Add configuration folders to your path
```
export PATH=$PATH:$HOME/ardupilot/Tools/autotest
export PATH=/usr/lib/ccache:$PATH
```
Set the params
```
cd ~/ardupilot/ArduCopter
sim_vehicle.py -w
```

## Installing ardupilot gazebo
Clone the repository
```
cd ~
git clone https://github.com/khancyr/ardupilot_gazebo.git
cd ardupilot_gazebo
git checkout dev
```

Build and install
```
mkdir build
cd build
cmake ..
make -j4
sudo make install
```

Edit .bashrc
```
echo 'source /usr/share/gazebo/setup.sh' >> ~/.bashrc
echo 'export GAZEBO_MODEL_PATH=~/ardupilot_gazebo/models' >> ~/.bashrc
. ~/.bashrc #source
```
## Installing Mavros

Install
```
sudo apt install ros-melodic-mavros ros-melodic-mavros-msgs ros-melodic-mavlink
. ~/.bashrc # refresh terminal
```

Download geographiclib dataset
```
sudo /opt/ros/melodic/lib/mavros/install_geographiclib_datasets.sh
```

## Installing Harpia
```
mkdir -p ~/catkin_ws/ # create a catkin workspace 
cd ~/catkin_ws
git clone https://gitlab.com/ufrjnautilus/harpia.git
mv harpia src
catkin_make
```

## Installing QGroundControl

Add user to supergroup and remove modemmanager (modemmanager controls mobile broadband and devices)
```
sudo usermod -a -G dialout $USER
sudo apt-get remove modemmanager
```

Download QGroundControl and run
```
wget https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.AppImage
chmod +x ./QGroundControl.AppImage 
./QGroundControl.AppImage
```

# Usage
## Square Trajectory
To make the drone take off, follow a square trajectory and then land, run

```
roslaunch iq_sim runway.launch
# New Terminal
~/catkin_ws/src/iq_sim/scripts/startsitl.sh
# New Terminal
roslaunch iq_sim apm.launch
# New Terminal 
rosrun iq_gnc square
```
Don't forget to source your catkin workspace.

## Empty world
To launch the drone in an empty world run
```
cd catkin_ws
. devel/setup.bash
roslaunch iq_sim runway.launch
``` 

Then, in a new terminal
```
~/catkin_ws/src/iq_sim/scripts/startsitl.sh
```

In another terminal start mavros
```
roslaunch iq_sim apm.launch
```

To be able to control it using command line arguments, run:
```
cd ardupilot/ArduCopter/

../Tools/autotest/sim_vehicle.py -f gazebo-iris --console --map

mode GUIDED
```

# TroubleShoot
## Ardupilot Run error
If Ardupilot is throwing errors, remember to source you catkin workspace and to add ardupilots autotest folder to path.
```
export PATH=$PATH:$HOME/ardupilot/Tools/autotest
export PATH=/usr/lib/ccache:$PATH
```
Also remember to download geographiclib dataset
```
/opt/ros/melodic/lib/mavros/install_geographiclib_datasets.sh
```

## Mavros subscriber error
If callbacks for mavros messages are not working properly then update genpy
```
sudo apt install ros-melodic-genpy
```
And then set the mavros stream rate
```
rosservice call /mavros/set_stream_rate 0 10 1
```

## Darknet ROS compiling error
Darknet_ros has an error when compiling with CUDA version 9.X. To fix it follow steps 9 and 10 of this tutorial:
https://davidwpearson.wordpress.com/2017/12/21/installing-nvidias-cuda-9-1-on-fedora-27/

# Source
[Iq Tutorials](https://github.com/Intelligent-Quads/iq_tutorials)
