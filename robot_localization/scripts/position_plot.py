#!/usr/bin/env python
import rospy
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from nav_msgs.msg import Odometry


class Plotter():
    def __init__(self):
        #self.sub = rospy.Subscriber("/mavros/global_position/local", Odometry, self.callback)
        self.sub = rospy.Subscriber(
            "/odometry/filtered", Odometry, self.callback)
        self.x = []
        self.y = []

    def callback(self, pos):
        position_x = pos.pose.pose.position.x
        position_y = pos.pose.pose.position.y

        self.x.append(position_x)
        self.y.append(position_y)

    def animate(self, i):
        plt.cla()
        plt.plot(self.x, self.y)

    def main(self):
        ani = FuncAnimation(plt.gcf(), self.animate, interval=1000)
        plt.show()


if __name__ == "__main__":
    rospy.init_node("bagplotter", anonymous=True)
    plot = Plotter()
    plot.main()
    rospy.spin()
