#!/usr/bin/env python
import rospy
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu
from tf.transformations import euler_from_quaternion, quaternion_from_euler


class Plotter():
    def __init__(self):
        #self.sub = rospy.Subscriber("/mavros/imu/data", Imu, self.callback)
        self.sub = rospy.Subscriber(
            "/odometry/filtered", Odometry, self.callback2)
        self.x = []
        self.y = []

    def callback(self, imu):

        quaternion_x = imu.orientation.x
        quaternion_y = imu.orientation.y
        quaternion_z = imu.orientation.z
        quaternion_w = imu.orientation.w

        (roll, pitch, yaw) = euler_from_quaternion(
            [quaternion_x, quaternion_y, quaternion_z, quaternion_w])

        self.x.append(len(self.x))
        # Change according to necessity of information
        self.y.append(roll)

    def callback2(self, odom):

        quaternion_x = odom.pose.pose.orientation.x
        quaternion_y = odom.pose.pose.orientation.y
        quaternion_z = odom.pose.pose.orientation.z
        quaternion_w = odom.pose.pose.orientation.w

        (roll, pitch, yaw) = euler_from_quaternion(
            [quaternion_x, quaternion_y, quaternion_z, quaternion_w])

        self.x.append(len(self.x))
        # Change according to necessity of information
        self.y.append(roll)

    def animate(self, i):
        plt.cla()
        plt.plot(self.x, self.y)

    def main(self):
        ani = FuncAnimation(plt.gcf(), self.animate, interval=1000)
        plt.show()


if __name__ == "__main__":
    rospy.init_node("bagplotter", anonymous=True)
    plot = Plotter()
    plot.main()
