# Robot_localization

## Usage
```$ roslaunch robot_localization ekf_harpia.launch```

For implementation of the Extended Kalman Filter

```$ roslaunch robot_localization ukf_harpia.launch```

For implementation of the Unscented Kalman Filter

## Test
In order to visualize noise from position or IMU, use either position_plot.py or imu_plot.py. Don't forget to change comentaries in order to plot the right information.

## Topics
Odometry is published on topic: 

/odometry/filtered

GPS on UTM coordinates is published on topic: 

/odometry/gps

## Reference
robot_localization is a package of nonlinear state estimation nodes. The package was developed by Charles River Analytics, Inc.

Please see documentation here: http://docs.ros.org/melodic/api/robot_localization/html/index.html



