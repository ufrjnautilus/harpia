#!/usr/bin/env python
import rospy
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
from nav_msgs.msg import Odometry


class Plotter():
    def __init__(self):
        self.sub = rospy.Subscriber("/mavros/global_position/local", Odometry, self.callback)
        self.x = []
        self.y = []

    def callback(self,Pos):
        Posx = Pos.pose.pose.position.x
        Posy = Pos.pose.pose.position.y
        self.x.append(Posx)
        self.y.append(-Posy)
        

    def animate(self,i):
        plt.cla()
        plt.plot(self.y,self.x)
        
    
    def main(self):
        ani = FuncAnimation(plt.gcf(),self.animate,interval=1000)
        plt.show()

if __name__ == "__main__":
    rospy.init_node("bagplotter", anonymous=True)
    plot = Plotter()
    plot.main()
    rospy.spin()
