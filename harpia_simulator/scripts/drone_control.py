import time
import rospy
from math import cos, sin, pi, atan2, sqrt
from std_msgs.msg import Float64, String
from mavros_msgs.msg import PositionTarget, State
from mavros_msgs.srv import CommandBool, SetMode, CommandTOL
from geometry_msgs.msg import Pose, PoseStamped, Point
from nav_msgs.msg import Odometry

class drone_control:
    
    @staticmethod
    def state_cb(msg):
        drone_control.current_state_g = msg

    @staticmethod
    def enu_2_local(current_pose_enu):
        x = current_pose_enu.pose.pose.position.x
        y = current_pose_enu.pose.pose.position.y
        z = current_pose_enu.pose.pose.position.z
        deg2rad = pi / 180.0
        current_pos_local = Point()
        current_pos_local.x = x * cos((drone_control.local_offset_g - 90) * deg2rad) - \
            y * sin((drone_control.local_offset_g - 90)*deg2rad)
        current_pos_local.y = x * sin((drone_control.local_offset_g - 90) * deg2rad) + \
            y * cos((drone_control.local_offset_g - 90)*deg2rad)
        current_pos_local.z = z

        return current_pos_local

    @staticmethod
    def pose_cb(msg):
        drone_control.current_pose_g = msg
        drone_control.enu_2_local(drone_control.current_pose_g)
        q0 = drone_control.current_pose_g.pose.pose.orientation.w
        q1 = drone_control.current_pose_g.pose.pose.orientation.x
        q2 = drone_control.current_pose_g.pose.pose.orientation.y
        q3 = drone_control.current_pose_g.pose.pose.orientation.z
        psi = atan2((2*(q0*q3 + q1*q2)), (1 - 2*((q2**2) + (q3**2))))
        drone_control.current_heading_g = psi*(180/pi) - drone_control.local_offset_g

    @staticmethod
    def get_current_location():
        current_pos_local = drone_control.enu_2_local(drone_control.current_pose_g)
        return current_pos_local

    @staticmethod
    def get_current_heading():
        return drone_control.current_heading_g

    @staticmethod
    def set_heading(heading):
        drone_control.local_desired_heading_g = heading
        heading = heading + drone_control.correction_heading_g + drone_control.local_offset_g
        
        rospy.loginfo("Desired Heading %f ", drone_control.local_desired_heading_g)
        yaw = heading*(pi/180)
        pitch = 0
        roll = 0

        cy = cos(yaw * 0.5)
        sy = sin(yaw * 0.5)
        cr = cos(roll * 0.5)
        sr = sin(roll * 0.5)
        cp = cos(pitch * 0.5)
        sp = sin(pitch * 0.5)

        qw = cy * cr * cp + sy * sr * sp
        qx = cy * sr * cp - sy * cr * sp
        qy = cy * cr * sp + sy * sr * cp
        qz = sy * cr * cp - cy * sr * sp

        drone_control.waypoint_g.pose.orientation.w = qw
        drone_control.waypoint_g.pose.orientation.x = qx
        drone_control.waypoint_g.pose.orientation.y = qy
        drone_control.waypoint_g.pose.orientation.z = qz

    @staticmethod
    def set_destination(x, y, z, psi):
        drone_control.set_heading(psi)
        deg2rad = (pi / 180)
        x_local = x*cos((drone_control.correction_heading_g + drone_control.local_offset_g - 90)*deg2rad) - \
                  y*sin((drone_control.correction_heading_g + drone_control.local_offset_g - 90)*deg2rad)
        y_local = x*sin((drone_control.correction_heading_g + drone_control.local_offset_g - 90)*deg2rad) + \
                  y*cos((drone_control.correction_heading_g + drone_control.local_offset_g - 90)*deg2rad)
        z_local = z

        x = x_local + drone_control.correction_vector_g.position.x + drone_control.local_offset_pose_g.x
        y = y_local + drone_control.correction_vector_g.position.y + drone_control.local_offset_pose_g.y
        z = z_local + drone_control.correction_vector_g.position.z + drone_control.local_offset_pose_g.z
        rospy.loginfo("Destination set to x: %f y: %f z: %f origin frame", x, y, z)

        drone_control.waypoint_g.pose.position.x = x
        drone_control.waypoint_g.pose.position.y = y
        drone_control.waypoint_g.pose.position.z = z

        drone_control.local_pos_pub.publish(drone_control.waypoint_g)

    @staticmethod 
    def wait4connect():
        rospy.loginfo("Waiting for FCU connection")
        while (not rospy.is_shutdown() and not drone_control.current_state_g.connected):
            time.sleep(0.1)
        if (drone_control.current_state_g.connected):
            rospy.loginfo("Connected to FCU")	
            return 0
        else:
            rospy.loginfo("Error connecting to drone")
            return -1	

    @staticmethod   
    def wait4start():
        rospy.loginfo("Waiting for user to set mode to GUIDED")
        while (not rospy.is_shutdown() and drone_control.current_state_g.mode != "GUIDED"):
            time.sleep(0.01)
        if (drone_control.current_state_g.mode == "GUIDED"):
            rospy.loginfo("Mode set to GUIDED. Mission starting")
            return 0
        else:
            rospy.loginfo("Error starting missionnot not ")
            return -1	
        
    @staticmethod
    def initialize_local_frame():
        rospy.loginfo("Initializing local coordinate system")
        drone_control.local_offset_g = 0
        for _ in range(1, 31):
            time.sleep(0.1)

            q0 = drone_control.current_pose_g.pose.pose.orientation.w
            q1 = drone_control.current_pose_g.pose.pose.orientation.x
            q2 = drone_control.current_pose_g.pose.pose.orientation.y
            q3 = drone_control.current_pose_g.pose.pose.orientation.z
            psi = atan2((2 * (q0 * q3 + q1 * q2)), (1 - 2 * ((q2**2) + (q3**2))))

            drone_control.local_offset_g += psi * (180 / pi)

            drone_control.local_offset_pose_g.x = drone_control.local_offset_pose_g.x + \
                                                  drone_control.current_pose_g.pose.pose.position.x
            drone_control.local_offset_pose_g.y = drone_control.local_offset_pose_g.y + \
                                                  drone_control.current_pose_g.pose.pose.position.y
            drone_control.local_offset_pose_g.z = drone_control.local_offset_pose_g.z + \
                                                  drone_control.current_pose_g.pose.pose.position.z
            
        drone_control.local_offset_pose_g.x = drone_control.local_offset_pose_g.x / 30
        drone_control.local_offset_pose_g.y = drone_control.local_offset_pose_g.y / 30
        drone_control.local_offset_pose_g.z = drone_control.local_offset_pose_g.z / 30
        drone_control.local_offset_g /= 30
        rospy.loginfo("Coordinate offset set")
        rospy.loginfo("the X' axis is facing: %f", drone_control.local_offset_g)

    @staticmethod
    def arm():
        drone_control.set_destination(0,0,0,0)
        for _ in range(100):
            drone_control.local_pos_pub.publish(drone_control.waypoint_g)
            time.sleep(0.01)
        
        rospy.loginfo("Arming drone")
        arm_request = drone_control.arming_client(True)
        while (not drone_control.current_state_g.armed and not arm_request.success and not rospy.is_shutdown()):
            time.sleep(0.1)
            arm_request = drone_control.arming_client(True)
            drone_control.local_pos_pub.publish(drone_control.waypoint_g)
        if (arm_request.response.success):
            rospy.loginfo("Arming Successful")	
            return 0
        else:
            rospy.loginfo("Arming failed with %d", arm_request.response.success)
            return -1	
        
    @staticmethod
    def takeoff(takeoff_alt):
        drone_control.set_destination(0,0,takeoff_alt,0)
        for _ in range(100):
            drone_control.local_pos_pub.publish(drone_control.waypoint_g)
            time.sleep(0.01)
        rospy.loginfo("Arming drone")
        arm_request = drone_control.arming_client(True)
        while (not drone_control.current_state_g.armed and not arm_request.success and not rospy.is_shutdown()):
            time.sleep(0.1)
            arm_request = drone_control.arming_client(True)
            drone_control.local_pos_pub.publish(drone_control.waypoint_g)
        
        if(arm_request.success):
            rospy.loginfo("Arming Successful")	
        else:
            rospy.loginfo("Arming failed with %d", arm_request.success)
            return -1	
        
        try:
            srv_takeoff = drone_control.takeoff_client(altitude=takeoff_alt)
            time.sleep(3)
            rospy.loginfo("takeoff sent %d", srv_takeoff.success)
        except rospy.ServiceException as e:
            rospy.logerr("Failed Takeoff: %s", e)
            return -2
        time.sleep(2)
        return 0

    @staticmethod
    def check_waypoint_reached(pos_tolerance=0.3, heading_tolerance=0.01):
        drone_control.local_pos_pub.publish(drone_control.waypoint_g)

        delta_x = abs(drone_control.waypoint_g.pose.position.x - drone_control.current_pose_g.pose.pose.position.x)
        delta_y = abs(drone_control.waypoint_g.pose.position.y - drone_control.current_pose_g.pose.pose.position.y)
        delta_z = 0 // (abs(drone_control.waypoint_g.pose.position.z - drone_control.current_pose_g.pose.pose.position.z) + 0.00001)
        d_mag = sqrt((delta_x**2) + (delta_y**2) + (delta_z**2))
        rospy.loginfo("dMag %f", d_mag)
        rospy.loginfo("current pose x %F y %f z %f", (drone_control.current_pose_g.pose.pose.position.x), (drone_control.current_pose_g.pose.pose.position.y), (drone_control.current_pose_g.pose.pose.position.z))
        rospy.loginfo("waypoint pose x %F y %f z %f", drone_control.waypoint_g.pose.position.x, drone_control.waypoint_g.pose.position.y,drone_control.waypoint_g.pose.position.z)
        # check orientation
        cos_err = cos(drone_control.current_heading_g*(pi/180)) - cos(drone_control.local_desired_heading_g*(pi/180))
        sin_err = sin(drone_control.current_heading_g*(pi/180)) - sin(drone_control.local_desired_heading_g*(pi/180))

        heading_err = sqrt( (cos_err**2) + (sin_err**2) )
        rospy.loginfo("headingErr: %f",heading_err)
        rospy.loginfo("current_heading: %f",drone_control.current_heading_g)
        if( d_mag < pos_tolerance and heading_err < heading_tolerance):
            return 1
        else:
            return 0

    @staticmethod
    def set_mode(mode):
        try:
            srv_setMode = drone_control.set_mode_client(base_mode=0, custom_mode=mode)
            rospy.loginfo("setmode send ok")
        except rospy.ServiceException as e:
            rospy.logerr("Failed SetMode: %s", e)
            return -1
        
    @staticmethod
    def land():
        try:
            srv_land = drone_control.land_client()
            rospy.loginfo("land sent %d", srv_land.success)
            return 0
        except rospy.ServiceException as e:
            rospy.logerr("Landing failed: %s", e)
            return -1

    current_state_g = State()
    current_pose_g = Odometry()
    correction_vector_g = Pose()
    local_offset_pose_g = Point()
    waypoint_g = PoseStamped()

    current_heading_g = 0
    local_offset_g = 0
    correction_heading_g = 0
    local_desired_heading_g = 0

    

    local_pos_pub = rospy.Publisher("/mavros/setpoint_position/local", PoseStamped, queue_size=10)
    currentPos = rospy.Subscriber("/mavros/global_position/local", Odometry, pose_cb.__func__)
    state_sub = rospy.Subscriber("/mavros/state", State, state_cb.__func__)
    arming_client = rospy.ServiceProxy("/mavros/cmd/arming", CommandBool)
    land_client = rospy.ServiceProxy("/mavros/cmd/land", CommandTOL)
    set_mode_client = rospy.ServiceProxy("/mavros/set_mode", SetMode)
    takeoff_client = rospy.ServiceProxy("/mavros/cmd/takeoff", CommandTOL)


if __name__ == "__main__": 
    rospy.init_node("gnc_node")
    try:
        drone_control.wait4connect()
        drone_control.wait4start()
        drone_control.initialize_local_frame()
        drone_control.takeoff(3)
        drone_control.set_destination(0,0,3,0)
        time.sleep(5)
        drone_control.land()
    except rospy.ROSInterruptException:
        pass