#!/bin/bash
# script to run the drone simulation
# experimental

if [ "$1" = "-d" ];then
    gnome-terminal -- roslaunch harpia_simulator runway_down_below.launch
else
    gnome-terminal -- roslaunch harpia_simulator runway.launch
fi

gnome-terminal -- ~/startsitl.sh
sleep 5
gnome-terminal -- roslaunch harpia apm.launch

