import drone_control
import rospy
import math


class square:
    def __init__(self):
        self.mode_r = 0
        self.waypoint_list = []
        self.body()

    def body(self):
        
        self.waypoint_list.append([10, 0, 10, 0])
        self.waypoint_list.append([10, -5, 10, 0])
        self.waypoint_list.append([5, -5, 10, 0])
        self.waypoint_list.append([5, 0, 10, 0])
        self.waypoint_list.append([0, -5, 10, 0])
        rospy.loginfo("R Added ")
        self.waypoint_list.append([0, -7, 10, 0])
        self.waypoint_list.append([10, -7, 10, 0])
        self.waypoint_list.append([10, -12, 10, 0])
        self.waypoint_list.append([10, -7, 10, 0])
        self.waypoint_list.append([5, -7, 10, 0])
        self.waypoint_list.append([5, -12, 10, 0])
        self.waypoint_list.append([5, -7, 10, 0])
        self.waypoint_list.append([0, -7, 10, 0])
        self.waypoint_list.append([0, -12, 10, 0])
        rospy.loginfo("E Added ")
        self.waypoint_list.append([0, -14, 10, 0])
        self.waypoint_list.append([10, -14, 10, 0])
        self.waypoint_list.append([10, -19, 10, 0])
        self.waypoint_list.append([0, -19, 10, 0])
        self.waypoint_list.append([0, -14, 10, 0])
        rospy.loginfo("D Added ")
        self.waypoint_list.append([0, -20, 10, 0])
        self.waypoint_list.append([10, -20, 10, 0])
        self.waypoint_list.append([0, -20, 10, 0])
        rospy.loginfo("I Added ")
        self.waypoint_list.append([0, -23, 10, 0])
        self.waypoint_list.append([10, -23, 10, 0])
        self.waypoint_list.append([10, -21, 10, 0])
        self.waypoint_list.append([10, -25, 10, 0])
        self.waypoint_list.append([10, -23, 10, 0])
        self.waypoint_list.append([0, -23, 10, 0])
        rospy.loginfo("T Added ")
        self.waypoint_list.append([0, -27, 10, 0])
        self.waypoint_list.append([10, -27, 10, 0])
        self.waypoint_list.append([0, -27, 10, 0])
        self.waypoint_list.append([0, -32, 10, 0])
        self.waypoint_list.append([10, -32, 10, 0])
        self.waypoint_list.append([0, -32, 10, 0])
        rospy.loginfo("U Added ")
        self.waypoint_list.append([0, -34, 10, 0])
        self.waypoint_list.append([0, -39, 10, 0])
        self.waypoint_list.append([5, -39, 10, 0])
        self.waypoint_list.append([5, -34, 10, 0])
        self.waypoint_list.append([10, -34, 10, 0])
        self.waypoint_list.append([10, -39, 10, 0])
        rospy.loginfo("S Added ")
        rate = rospy.Rate(2.0)
        counter = 0
        while not rospy.is_shutdown():
            rate.sleep()
            if self.mode_r != 0:
                drone_control.land()
                rospy.loginfo("Landing Started")
                break

            if drone_control.check_waypoint_reached() != 1:
                continue

            rospy.loginfo("we got to waypoint")
            if counter < len(self.waypoint_list):
                rospy.loginfo("go to waypoint")
                drone_control.set_destination(
                    self.waypoint_list[counter][0], self.waypoint_list[counter][1], self.waypoint_list[counter][2], self.waypoint_list[counter][3])
                counter = counter+1
            else:
                drone_control.land()


if __name__ == "__main__":
    rospy.init_node('drone_square')
    drone_control = drone_control.drone_control()
    drone_control.wait4connect()
    drone_control.wait4start()
    drone_control.initialize_local_frame()
    drone_control.takeoff(3)
    drone = square()
    rospy.spin()
