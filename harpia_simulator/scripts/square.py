import drone_control
import rospy
import math


class square:
    def __init__(self):
        self.mode_r = 0
        self.waypoint_list = []
        self.body()

    def body(self):
        for i in range(600):
            self.waypoint_list.append([10, 0, 10, 0])
            self.waypoint_list.append([10, 10, 10, 0])
            self.waypoint_list.append([0, 10, 10, 0])
            self.waypoint_list.append([0, 0, 10, 0])
            rospy.loginfo("Square Added "+str(i))

        rate = rospy.Rate(2.0)
        counter = 0
        while not rospy.is_shutdown():
            rate.sleep()
            if self.mode_r != 0:
                drone_control.land()
                rospy.loginfo("Landing Started")
                break

            if drone_control.check_waypoint_reached() != 1:
                continue

            rospy.loginfo("we got to waypoint")
            if counter < len(self.waypoint_list):
                rospy.loginfo("go to waypoint")
                drone_control.set_destination(
                    self.waypoint_list[counter][0], self.waypoint_list[counter][1], self.waypoint_list[counter][2], self.waypoint_list[counter][3])
                counter = counter+1
            else:
                drone_control.land()


if __name__ == "__main__":
    rospy.init_node('drone_square')
    drone_control = drone_control.drone_control()
    drone_control.wait4connect()
    drone_control.wait4start()
    drone_control.initialize_local_frame()
    drone_control.takeoff(3)
    drone = square()
    rospy.spin()
