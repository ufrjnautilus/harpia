#!/bin/bash
# script to install gazebo simulation
# experimental

# Ardupilot
cd ~
git clone https://github.com/ArduPilot/ardupilot.git
cd ardupilot
git checkout Copter-3.6
git submodule update --init --recursive

sudo apt install python-matplotlib python-serial python-wxgtk3.0 python-wxtools python-lxml python-scipy python-opencv ccache gawk python-pip python-pexpect
sudo pip install future pymavlink MAVProxy

export PATH=$PATH:$HOME/ardupilot/Tools/autotest
export PATH=/usr/lib/ccache:$PATH

cd ~/ardupilot/ArduCopter
sim_vehicle.py -w

# Ardupilot Gazebo
cd ~
git clone https://github.com/khancyr/ardupilot_gazebo.git
cd ardupilot_gazebo
git checkout dev

mkdir build
cd build
cmake ..
make -j4
sudo make install

echo 'source /usr/share/gazebo/setup.sh' >> ~/.bashrc
echo 'export GAZEBO_MODEL_PATH=~/ardupilot_gazebo/models' >> ~/.bashrc
. ~/.bashrc #source

# Mavros
sudo apt install ros-melodic-mavros ros-melodic-mavros-msgs ros-melodic-mavlink
. ~/.bashrc # refresh terminal
sudo /opt/ros/melodic/lib/mavros/install_geographiclib_datasets.sh

if [ -d "catkin_ws" ]; then 
    cd catkin_ws
    if [ -d "src" ]; then
        cd src
    else
        echo "catkin_ws/src not found. Creating src folder"
        mkdir src
    fi
    echo "catkin_ws existis. Cloning IQ simulation into catkin_ws/src"
else
    echo "catkin_ws does not existis. Creating catkin_ws/src and cloning IQ simulation into catkin_ws/src"
    mkdir -p ~/catkin_ws/src
fi
cd ~/catkin_ws
catkin init
cd src
git clone https://github.com/Intelligent-Quads/iq_sim.git
cd ../
catkin_make

# IQ GNX Ros
cd catkin_ws/src
git clone https://github.com/Intelligent-Quads/iq_gnc.git
cd ..
catkin_make
. devel/setup.bash

#QGroundControl
cd
sudo usermod -a -G dialout $USER
sudo apt-get remove modemmanager
wget https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.AppImage
chmod +x ./QGroundControl.AppImage 
