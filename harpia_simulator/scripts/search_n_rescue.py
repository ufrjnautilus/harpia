import drone_control
import rospy
from darknet_ros_msgs.msg import BoundingBoxes
import math


class search_n_rescue:
    def __init__(self):
        self.mode_r = 0
        self.waypoint_list = []
        self.sub = rospy.Subscriber(
            "darknet_ros/bounding_boxes", BoundingBoxes, self.callback)
        self.body()

    def callback(self, msg):
        for element in msg.bounding_boxes:
            rospy.loginfo(element.Class + " detected")
            if element.Class == "person":
                self.mode_r = 1
                rospy.loginfo("Person found. Rescue operation starting ...")

    def body(self):
        rng = 50
        spacing = 10

        for i in range(5):
            row = i*2
            self.waypoint_list.append([row*spacing, 0, 10, 0])
            rospy.loginfo("Waypoint Added" + str(i))
            self.waypoint_list.append([row*spacing, rng, 10, 0])
            rospy.loginfo("Waypoint Added" + str(i))
            self.waypoint_list.append([(row+1)*spacing, rng, 10, 0])
            rospy.loginfo("Waypoint Added" + str(i))
            self.waypoint_list.append([(row+1)*spacing, 0, 10, 0])
            rospy.loginfo("Waypoint Added"+str(i))

        rate = rospy.Rate(2.0)
        counter = 0
        while not rospy.is_shutdown():
            rate.sleep()
            if self.mode_r != 0:
                drone_control.land()
                rospy.loginfo("Landing Started")
                break

            if drone_control.check_waypoint_reached() != 1:
                continue

            rospy.loginfo("we got to waypoint")
            if counter < len(self.waypoint_list):
                rospy.loginfo("go to waypoint")
                drone_control.set_destination(
                    self.waypoint_list[counter][0], self.waypoint_list[counter][1],
                    self.waypoint_list[counter][2], self.waypoint_list[counter][3]
                )
                counter = counter+1
            else:
                drone_control.land()


if __name__ == "__main__":
    rospy.init_node('search_n_rescue')
    drone_control = drone_control.drone_control()
    drone_control.wait4connect()
    drone_control.wait4start()
    drone_control.initialize_local_frame()
    drone_control.takeoff(3)
    drone = search_n_rescue()
    rospy.spin()
