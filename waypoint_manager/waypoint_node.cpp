#include <ros/ros.h>
#include "../iq_gnc/include/gnc_functions.hpp"


void callback(){

    std::vector<gnc_api_waypoint> waypointList;

	gnc_api_waypoint nextWayPoint;

    
    nextWayPoint.x = 0;
    nextWayPoint.y = 0;
    nextWayPoint.z = 0;
    nextWayPoint.psi = 0;
    waypointList.push_back(nextWayPoint);	


}

int main (int argc,char **argv){

    ros::init(argc,argv,"Waypoints");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("topic",1, callback);
    init_publisher_subscriber(n);
    
    ros::Rate rate(2.0);
    int counter = 0;
    while(ros::ok()){  
        ros::spinOnce();
        rate.sleep();
        if(check_waypoint_reached(.3) == 1)
        {
            if(counter < waypointList.size())
            {
                set_destination(waypointList[counter].x, waypointList[counter].y, waypointList[counter].z, waypointList[counter].psi);
                counter++;
            }
        }
	}


    return 0;

}