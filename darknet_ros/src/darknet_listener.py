import rospy
from robosub_msgs.msg import DetectionArray


def callback(data):
    for d in data.boxes:
        rospy.loginfo("label: %s, x: %f, y: %f, width: %f, height: %f", 
                         d.label, d.x, d.y, d.w, d.h)


def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("Darknet", DetectionArray, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    listener()