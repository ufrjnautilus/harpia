#!/usr/bin/env python

from __future__ import print_function, division

import os

# change location to scripts. Remove when ros parameters are implemented
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

import rospy
from robosub_msgs.msg import Detection, DetectionArray
from sensor_msgs.msg import Image
from std_msgs.msg import String
import cv2
from cv_bridge import CvBridge, CvBridgeError

from ctypes import *
import math
import random
# import os
import time
import darknet


netMain = None
metaMain = None
darknet_image = None
bridge = None
detections_pub = None
image_pub = None

camera_selection = "front"

def convert_back(x, y, w, h):
    '''
    Converts the parameters of the boxes to the opencv format.
    '''
    xmin = int(round(x - (w / 2)))
    xmax = int(round(x + (w / 2)))
    ymin = int(round(y - (h / 2)))
    ymax = int(round(y + (h / 2)))
    return xmin, ymin, xmax, ymax


def cv_draw_boxes(detections, img):
    '''
    Draws the detected boxes.
    '''
    for detection in detections:
        x, y, w, h = detection[2][0],\
            detection[2][1],\
            detection[2][2],\
            detection[2][3]
        xmin, ymin, xmax, ymax = convert_back(
            float(x), float(y), float(w), float(h))
        pt1 = (xmin, ymin)
        pt2 = (xmax, ymax)
        cv2.rectangle(img, pt1, pt2, (0, 255, 0), 1)
        cv2.putText(img,
                    detection[0].decode() +
                    " [" + str(round(detection[1] * 100, 2)) + "]",
                    (pt1[0], pt1[1] - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    [0, 255, 0], 2)
    return img


def load_network():
    '''
    Initializes the darknet.
    '''
    # use ros param
    configPath = "./cfg/yolov3-tiny-obj.cfg"
    weightPath = "./backup/yolov3-tiny-obj_best.weights"
    metaPath = "./data/obj.data"

    if not os.path.exists(configPath):
        raise ValueError("Invalid config path `" +
                         os.path.abspath(configPath)+"`")
    if not os.path.exists(weightPath):
        raise ValueError("Invalid weight path `" +
                         os.path.abspath(weightPath)+"`")
    if not os.path.exists(metaPath):
        raise ValueError("Invalid data file path `" +
                         os.path.abspath(metaPath)+"`")
    
    netMain = darknet.load_net_custom(configPath.encode(
        "ascii"), weightPath.encode("ascii"), 0, 1)  # batch size = 1

    metaMain = darknet.load_meta(metaPath.encode("ascii"))

    # Create an image we reuse for each detect
    darknet_image = darknet.make_image(darknet.network_width(netMain),
                                    darknet.network_height(netMain),3)

    return netMain, metaMain, darknet_image


def resize_frame(frame, netMain):
    '''
    Resizes the images to darknet's cfg size. 
    '''
    #frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame_rgb = frame
    frame_resized = cv2.resize(frame_rgb,
                                (darknet.network_width(netMain),
                                darknet.network_height(netMain)),
                                interpolation=cv2.INTER_LINEAR)

    return frame_resized


def detect(netMain, metaMain, darknet_image, frame_resized):
    '''
    Returns the detections made by darknet and a ros detection message type.
    '''
    darknet.copy_image_from_bytes(darknet_image, frame_resized.tobytes())

    detections = darknet.detect_image(netMain, metaMain, darknet_image, thresh=0.25)

    detection_array = DetectionArray()

    for d in detections:
        box = Detection()

        box.label = d[0].decode()

        box.x = d[2][0] / darknet.network_width(netMain) # create a variable for the image width
        box.y = d[2][1] / darknet.network_height(netMain) # create a variable for the image height as well
        box.w = d[2][2] / darknet.network_width(netMain)
        box.h = d[2][3] / darknet.network_height(netMain)

        detection_array.boxes.append(box)
        
    return detections, detection_array
    

def detections_img(frame_resized, detections, netMain):
    '''
    Draws the boxes on the camera image.  
    '''
    image = cv_draw_boxes(detections, frame_resized)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    return image


def selection_callback(data):
    '''
    Sets camera selection to front when data='front' and bottom when data='bottom'.
    '''
    global camera_selection

    camera_selection = data.data
    

def front_callback(data):
    '''
    Calls run_darknet using images from front camera.
    '''
    if camera_selection == "front":
        run_darknet(data)
    
    else:
        pass


def bottom_callback(data):
    '''
    Calls run_darknet using images from bottom camera.
    '''
    if camera_selection == "bottom":
        run_darknet(data)

    else:
        pass


def run_darknet(data):
    '''
    Runs all darknet functions
    '''

    prev_time = time.time()

    frame = bridge.imgmsg_to_cv2(data)
    frame_resized = resize_frame(frame, netMain)

    detections, detection_array = detect(netMain, metaMain, darknet_image, frame_resized)
    detections_pub.publish(detection_array)

    image = detections_img(frame_resized, detections, netMain)

    image_pub.publish(bridge.cv2_to_imgmsg(image, "bgr8"))

    print(1/(time.time()-prev_time))


def YOLO():
    '''
    Main YOLO loop 
    '''
    global netMain, metaMain, darknet_image, bridge, detections_pub, image_pub
    netMain, metaMain, darknet_image = load_network()
    
    detections_pub = rospy.Publisher('darknet', DetectionArray, queue_size=10)
    image_pub = rospy.Publisher("darknet_image", Image, queue_size=10)

    rospy.init_node('DarknetNode', anonymous=True)

    rospy.Subscriber('/change_camera', String, selection_callback)

    rospy.Subscriber('/camera/left/image_raw', Image, front_callback)
    rospy.Subscriber('/camera/bottom/left/image_raw', Image, bottom_callback)
    
    bridge = CvBridge()

    print("Starting the YOLO loop...")

    rospy.spin()
    

if __name__ == "__main__":
    YOLO()
