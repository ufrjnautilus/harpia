# Darknet
darknet_ros is a package that connects darknet and ros. 


## Installation and Setup

First install [OpenCV](https://www.learnopencv.com/install-opencv-3-4-4-on-ubuntu-18-04/) < 4.0 

Open darknet/src/Makefile and check if make options and architecture are the correct ones. By default they are set to match Jetson Tx2.  

Then go to /darknet/src directory and run
```
make
```

Complete guide about instalation and compiling  [here](src/README.md)

To set the parameters of the network download from Nautilus' [drive](https://drive.google.com/drive/u/1/folders/10WOb9LUbSqk34gpwcCDsbvG3ZxX2oFyA) the .weights files and place them inside /src/backup. You can use the weights from coco dataset or from our custom objects.

### Custom objects

Go to darknet_video.py and darknet_video_from_topic.py and in function load_network() set the configurations like this:
```
    configPath = "./cfg/yolov3-tiny-obj.cfg"
    weightPath = "./backup/yolov3-tiny-obj_best.weights"
    metaPath = "./data/obj.data"
```
### Coco dataset

set configurations to
```
    configPath = "./cfg/yolov3-tiny.cfg"
    weightPath = "./backup/yolov3-tiny.weights"
    metaPath = "./cfg/coco.data"
```


## Usage
To start it using images from the computer webcam run 

```bash
roscore

rosrun darknet_ros darknet_video.py
```
To get images from topics 

```
roslaunch darknet_ros darknet.launch
```
or

```
roscore

rosrun darknet_ros darknet_video_from_topic.py
``` 

This node can use images from topics /camera/left/image_raw and /camera/bottom/left/image_raw. It will always start getting the images from the front camera. To read from bottom camera publish string "bottom" to the topic /change_camera. To read from front camera again publish "front".

 
## Message
The darknet_video node publishes at /darknet and /darknet_image topics. 

To see the detections message run
```bash
rostopic echo /darknet
```

The published message is an array of type Detections, containing the following parameters:

* string label ----- object number
* float64 x -------- X axys relative to box center 
* float64 y -------- Y axys 
* float64 w -------- box width
* float64 h -------- box hight

To see the image with detection boxes run
```bash
rosrun image_view image_view image:=/darknet_image
```

## Dependencies
You must have the following package installed [robosub_messages](https://gitlab.com/nautilusufrj/brhue/robosub_msgs) 




